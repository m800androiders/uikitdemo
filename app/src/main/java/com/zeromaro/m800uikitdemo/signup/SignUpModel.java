package com.zeromaro.m800uikitdemo.signup;

import android.support.annotation.IntDef;

import com.m800.verification.M800CountryCode;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Aurimas on 22/12/2017.
 */

public class SignUpModel {
    public static final int NEW = 0;
    public static final int ASK_FOR_VERIFICATION_CODE = 1;
    public static final int VERIFY_CODE = 2;
    public static final int PENDING = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NEW, ASK_FOR_VERIFICATION_CODE, VERIFY_CODE, PENDING})
    @interface VerificationStatus {}

    private String mPhoneNo;
    private M800CountryCode mM800CountryCode;
    private int mVerificationStatus;

    private VerificationStatusChangedListener mVerificationStatusChangedListener;

    public SignUpModel() {
        //For now only HK numbers are allowed
        mM800CountryCode = new M800CountryCode();
        mM800CountryCode.setCallCode(852);
        mM800CountryCode.setCountryCode("HK");

        setVerificationStatus(ASK_FOR_VERIFICATION_CODE);
    }

    public void setVerificationStatusChangedListener(VerificationStatusChangedListener verificationStatusChangedListener) {
        mVerificationStatusChangedListener = verificationStatusChangedListener;
    }

    public String getPhoneNo() {
        return mPhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        mPhoneNo = phoneNo;
    }

    public M800CountryCode getM800CountryCode() {
        return mM800CountryCode;
    }

    public void resetVerificationStatus() {
        setPhoneNo("");
        setVerificationStatus(NEW);
    }

    public int getVerificationStatus() {
        return mVerificationStatus;
    }

    public void setVerificationStatus(@VerificationStatus int verificationStatus) {
        mVerificationStatus = verificationStatus;
        if (mVerificationStatusChangedListener != null) {
            mVerificationStatusChangedListener.onVerificationStatusChanged(verificationStatus);
        }
    }

    public interface VerificationStatusChangedListener {
        void onVerificationStatusChanged(@VerificationStatus int verificationStatus);
    }
}
