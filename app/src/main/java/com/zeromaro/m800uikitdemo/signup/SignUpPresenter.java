package com.zeromaro.m800uikitdemo.signup;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.m800.sdk.IM800Management;
import com.m800.sdk.M800Error;
import com.m800.sdk.M800SDK;
import com.m800.sdk.M800UserIdentity;
import com.m800.sdk.M800ValidationToken;
import com.m800.verification.M800VerificationManager;
import com.m800.verification.M800VerificationManagerCallback;
import com.m800.verification.M800VerificationType;
import com.m800.verification.MultiDeviceCode;
import com.m800.verification.PhoneNumberInfo;

import java.lang.ref.WeakReference;

import static com.zeromaro.m800uikitdemo.signup.SignUpModel.PENDING;

/**
 * Created by Aurimas on 22/12/2017.
 */

public class SignUpPresenter implements SignUpContract.Presenter {

    private M800VerificationManager mM800VerificationManager;
    private IM800Management mIM800Management;

    private SignUpContract.View mView;
    private SignUpModel mSignUpModel;

    private M800VerificationCallback mM800VerificationCallback;
    private SignUpCallback mSignUpCallback;

    public SignUpPresenter(SignUpContract.View view, M800VerificationManager m800VerificationManager, SignUpModel signUpModel) {
        mView = view;
        mM800VerificationManager = m800VerificationManager;
        mSignUpModel = signUpModel;

        mIM800Management = M800SDK.getInstance().getManagement();

        mM800VerificationCallback = new M800VerificationCallback(mView, this, mSignUpModel);
        mSignUpCallback = new SignUpCallback(view, this);
    }

    @Override
    public void addVerificationCallback() {
        mM800VerificationManager.addCallback(mM800VerificationCallback);
    }

    @Override
    public void removeVerificationCallback() {
        mM800VerificationManager.removeCallback(mM800VerificationCallback);
    }

    @Override
    public void requestSMSVerificationCode() {
        mSignUpModel.setVerificationStatus(SignUpModel.PENDING);
        mM800VerificationManager.startSmsVerification(mSignUpModel.getM800CountryCode(), mSignUpModel.getPhoneNo());
    }

    @Override
    public void verifySMSCode(@NonNull String verificationCode) {
        if (mM800VerificationManager.isPhoneVerificationInProgress()) {
            mSignUpModel.setVerificationStatus(PENDING);
            mM800VerificationManager.verifySmsCode(verificationCode);
            mView.showMessageToast("Verifying, please wait...");
        } else {
            mView.showMessageToast("Time expired, please try again");
            mSignUpModel.setVerificationStatus(SignUpModel.ASK_FOR_VERIFICATION_CODE);
        }
    }

    @Override
    public void resetForm() {
        mSignUpModel.resetVerificationStatus();
        if (mM800VerificationManager.isPhoneVerificationInProgress()) {
            mM800VerificationManager.abortPhoneVerification();
        }
    }

    private void signUp(@NonNull M800ValidationToken m800ValidationToken) {
        M800UserIdentity identity = new M800UserIdentity(mSignUpModel.getPhoneNo(),
                mSignUpModel.getM800CountryCode().getCountryCode(),
                M800UserIdentity.Type.PhoneNumber);

        mIM800Management.signup(identity, m800ValidationToken,
                IM800Management.M800Language.M800LanguageEnglish,
                mSignUpCallback);
    }

    private static class M800VerificationCallback extends M800VerificationManagerCallback {

        private WeakReference<SignUpContract.View> mViewWeakReference;
        private WeakReference<SignUpPresenter> mSignUpPresenterWeakReference;
        private SignUpModel mSignUpModel;

        public M800VerificationCallback(SignUpContract.View view, SignUpPresenter signUpPresenter, SignUpModel signUpModel) {
            super();
            mViewWeakReference = new WeakReference<SignUpContract.View>(view);
            mSignUpPresenterWeakReference = new WeakReference<SignUpPresenter>(signUpPresenter);
            mSignUpModel = signUpModel;
        }

        @Override
        public void onLoadPhoneNumberInfoSuccess(PhoneNumberInfo phoneNumberInfo) {
            super.onLoadPhoneNumberInfoSuccess(phoneNumberInfo);
        }

        @Override
        public void onLoadPhoneNumberInfoFailed(int i, String s) {
            super.onLoadPhoneNumberInfoFailed(i, s);
        }

        @Override
        public void onError(M800VerificationType m800VerificationType, int i, String s) {
            if (mViewWeakReference.get() != null) {
                mViewWeakReference.get().showMessageToast(s);
                mSignUpModel.setVerificationStatus(SignUpModel.ASK_FOR_VERIFICATION_CODE);
            }
        }

        @Override
        public void onSuccess(M800VerificationType m800VerificationType, String requestId) {
            M800ValidationToken token = new M800ValidationToken(requestId, null,
                    M800ValidationToken.ValidationSource.M800VerificationSDK);

            if (mSignUpPresenterWeakReference.get() != null) {
                mSignUpPresenterWeakReference.get().signUp(token);
            }
        }

        @Override
        public void onCodeCheckedIncorrect(String s) {
            if (mViewWeakReference.get() != null) {
                mViewWeakReference.get().showMessageToast("Code is incorrect, please try again...");
                mSignUpModel.setVerificationStatus(SignUpModel.VERIFY_CODE);
            }
        }

        @Override
        public void onWaitingToReceiveCode(int i) {
            if (mViewWeakReference.get() != null) {
                mViewWeakReference.get().showMessageToast("Request sent, please wait...");
                mSignUpModel.setVerificationStatus(SignUpModel.VERIFY_CODE);
            }
        }

        @Override
        public void onDeviceCodeGenerated(MultiDeviceCode multiDeviceCode) {
            super.onDeviceCodeGenerated(multiDeviceCode);
        }

        @Override
        public void onSuccess(MultiDeviceCode multiDeviceCode, String s, String s1, String s2) {
            super.onSuccess(multiDeviceCode, s, s1, s2);
        }

        @Override
        public boolean onDeviceCodeExpired(MultiDeviceCode multiDeviceCode) {
            return super.onDeviceCodeExpired(multiDeviceCode);
        }

        @Override
        public void onNewDeviceValidated() {
            super.onNewDeviceValidated();
        }

        @Override
        public void onNewDeviceValidationFailure(int i, String s) {
            super.onNewDeviceValidationFailure(i, s);
        }
    }

    private static class SignUpCallback implements IM800Management.M800ManagementCallback {

        private WeakReference<SignUpContract.View> mViewWeakReference;
        private WeakReference<SignUpPresenter> mSignUpPresenterWeakReference;


        public SignUpCallback(SignUpContract.View view, SignUpPresenter signUpPresenter) {
            mViewWeakReference = new WeakReference<SignUpContract.View>(view);
            mSignUpPresenterWeakReference = new WeakReference<SignUpPresenter>(signUpPresenter);
        }

        @Override
        public void complete(boolean isSuccess, M800Error error, Bundle userInfo) {
            if (isSuccess) {
                if (mViewWeakReference.get() != null) {
                    mViewWeakReference.get().goToMainActivity();
                }
            } else {
                if (mViewWeakReference.get() != null) {
                    mViewWeakReference.get().showMessageToast(error.toString());
                    if (mSignUpPresenterWeakReference.get() != null) {
                        mSignUpPresenterWeakReference.get().mSignUpModel.resetVerificationStatus();
                    }
                }
            }
        }
    }
}
