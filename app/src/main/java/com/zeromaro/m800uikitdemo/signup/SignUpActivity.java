package com.zeromaro.m800uikitdemo.signup;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.m800.verification.M800VerificationClient;
import com.m800.verification.M800VerificationManager;
import com.zeromaro.m800uikitdemo.R;
import com.zeromaro.m800uikitdemo.main.MainActivity;

import static com.zeromaro.m800uikitdemo.signup.SignUpModel.ASK_FOR_VERIFICATION_CODE;
import static com.zeromaro.m800uikitdemo.signup.SignUpModel.PENDING;
import static com.zeromaro.m800uikitdemo.signup.SignUpModel.NEW;
import static com.zeromaro.m800uikitdemo.signup.SignUpModel.VERIFY_CODE;

public class SignUpActivity extends AppCompatActivity implements SignUpContract.View,
        View.OnClickListener, SignUpModel.VerificationStatusChangedListener {

    private static final String TAG = "SignUpActivity";
    private static final int REQUEST_CODE_PERMISSION = 1;

    private final String[] mPermissionsArray = {
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_CONTACTS};

    private EditText mEtPhoneNo, mEtVerification;
    private Button mBtnProceed;
    private MenuItem mMenuItemSignUpReset;

    private SignUpContract.Presenter mSignUpPresenter;
    private SignUpModel mSignUpModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        checkPermissions();

        M800VerificationManager m800VerificationManager = M800VerificationClient.getVerificationManager(getApplicationContext());
        mSignUpModel = new SignUpModel();
        mSignUpModel.setVerificationStatusChangedListener(this);
        mSignUpPresenter = new SignUpPresenter(this, m800VerificationManager, mSignUpModel);
        mSignUpPresenter.addVerificationCallback();

        mEtPhoneNo = (EditText) findViewById(R.id.et_sign_up);
        mEtVerification = (EditText) findViewById(R.id.et_verification_code);
        mBtnProceed = (Button) findViewById(R.id.btn_proceed);
        mBtnProceed.setOnClickListener(this);

        mSignUpModel.setVerificationStatus(ASK_FOR_VERIFICATION_CODE);
    }

    @Override
    protected void onDestroy() {
        mSignUpPresenter.removeVerificationCallback();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (mSignUpModel.getVerificationStatus()) {
            case NEW:
            case ASK_FOR_VERIFICATION_CODE:
                mSignUpModel.setPhoneNo(mEtPhoneNo.getText().toString());
                mSignUpPresenter.requestSMSVerificationCode();
                break;
            case VERIFY_CODE:
                mSignUpPresenter.verifySMSCode(mEtVerification.getText().toString());
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        mMenuItemSignUpReset = menu.findItem(R.id.menu_signup_reset);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_signup_reset:
                mSignUpPresenter.resetForm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showMessageToast(String message) {
        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goToMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onVerificationStatusChanged(@SignUpModel.VerificationStatus int verificationStatus) {
        switch (verificationStatus) {
            case NEW:
                mMenuItemSignUpReset.setEnabled(true);
                mEtPhoneNo.setText("");
            case ASK_FOR_VERIFICATION_CODE:
                mEtVerification.setText("");
                mBtnProceed.setEnabled(true);
                mBtnProceed.setText("Get Code");
                mEtPhoneNo.setEnabled(true);
                mEtPhoneNo.requestFocus();
                mEtVerification.setEnabled(false);
                break;
            case VERIFY_CODE:
                mMenuItemSignUpReset.setEnabled(true);
                mBtnProceed.setEnabled(true);
                mBtnProceed.setText("Verify");
                mEtPhoneNo.setEnabled(false);
                mEtVerification.setEnabled(true);
                mEtVerification.requestFocus();
                break;
            case PENDING:
                mMenuItemSignUpReset.setEnabled(false);
                mBtnProceed.setEnabled(false);
                mEtPhoneNo.setEnabled(false);
                mEtVerification.setEnabled(false);
                break;
        }
    }


    //->TODO improve permission handling
    private void checkPermissions() {
        if (!hasPermissions(this, mPermissionsArray)) {
            ActivityCompat.requestPermissions(this, mPermissionsArray, REQUEST_CODE_PERMISSION);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    //TODO improve permission handling <-
}
