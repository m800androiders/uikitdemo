package com.zeromaro.m800uikitdemo.signup;

/**
 * Created by Aurimas on 22/12/2017.
 */

public interface SignUpContract {
    interface Presenter{
        void addVerificationCallback();
        void removeVerificationCallback();
        void requestSMSVerificationCode();
        void verifySMSCode(String verificationCode);
        void resetForm();
    }

    interface View{
        void showMessageToast(String message);
        void goToMainActivity();
    }
}
