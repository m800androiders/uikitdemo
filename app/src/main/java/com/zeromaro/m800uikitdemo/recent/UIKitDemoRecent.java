package com.zeromaro.m800uikitdemo.recent;

import android.view.MenuItem;
import android.widget.Toast;

import com.m800.uikit.recent.M800RecentActivity;
import com.m800.uikit.widget.toptoolbar.M800TopToolbar;

/**
 * Demonstration of overriding clickable features in UIKit (Recent screen)
 */

public class UIKitDemoRecent extends M800RecentActivity {
    @Override
    public void onTopToolbarSearchIconClick(M800TopToolbar m800TopToolbar, MenuItem menuItem) {
        Toast.makeText(this, "Clicked on Search, DEMO behaviour", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMucRoomProfileIconClick(String groupRoomId) {
        Toast.makeText(this, "Clicked on icon, DEMO behaviour", Toast.LENGTH_SHORT).show();
    }
}
