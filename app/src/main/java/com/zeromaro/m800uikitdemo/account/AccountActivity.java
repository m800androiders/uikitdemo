package com.zeromaro.m800uikitdemo.account;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.m800.sdk.M800SDK;
import com.m800.sdk.common.M800PacketError;
import com.m800.sdk.contact.IM800UserProfile;
import com.m800.sdk.credit.IM800CreditManager;
import com.m800.sdk.user.IM800AccountManager;

import java.util.Calendar;
import java.util.Currency;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.m800.uikit.util.DialogUtils;
import com.zeromaro.m800uikitdemo.R;

/**
 * TAKEN FROM M800 SDK DEMO APP with little modifications
 */
public class AccountActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String DEBUG_TAG = AccountActivity.class.getSimpleName();

    private static final int MAX_NAME_LENGTH = 20;
    private static final int MAX_STATUS_LENGTH = 25;

    private static final int REQ_SELECT_PROFILE = 100;
    private static final int REQ_SELECT_COVER = 101;
    private static final int REQ_SELECT_VIDEO = 102;

    private static final int MSG_FILE_UPLOAD_STARTED = 1000;
    private static final int MSG_FILE_UPLOAD_PROGRESS = 1001;
    private static final int MSG_FILE_UPLOAD_ENDED = 1002;

    private static final IM800UserProfile.Gender[] GENDERS = {IM800UserProfile.Gender.Male, IM800UserProfile.Gender.Female};

    private ImageView mCoverImageView, mProfileImageView, mVideoCallerThumbImageView;
    private ProgressBar mCoverImageProgressBar, mProfileImageProgressBar, mVideoThumbProgressBar;
    private TextView mHeaderNameTextView, mHeaderStatusTextView;
    private TextView mNameTextView, mStatusTextView, mPhoneNumberTextView,
            mEmailTextView, mGenderTextView, mBirthTextView, mBalanceTextView;

    private IM800AccountManager mAccountManager;
    private IM800CreditManager mCreditManager;
    private MyUserProfile mUserProfile;
    private ArrayAdapter<IM800UserProfile.Gender> mGenderAdapter;
    private ArrayAdapter<String> mMediaEditingAdapter;
    private ExecutorService mVideoProcessExecutor;

    private DialogUtils mDialogUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        mDialogUtils = new DialogUtils(this);

        mCoverImageView = (ImageView) findViewById(R.id.iv_cover);
        mProfileImageView = (ImageView) findViewById(R.id.iv_profile);
        mVideoCallerThumbImageView = (ImageView) findViewById(R.id.iv_video_caller_id);

        mCoverImageProgressBar = (ProgressBar) findViewById(R.id.cover_progressBar);
        mProfileImageProgressBar = (ProgressBar) findViewById(R.id.profile_progressBar);
        mVideoThumbProgressBar = (ProgressBar) findViewById(R.id.video_progressBar);

        mHeaderNameTextView = (TextView) findViewById(R.id.tv_name);
        mHeaderStatusTextView = (TextView) findViewById(R.id.tv_status);

        mNameTextView = (TextView) findViewById(R.id.tv_account_name);
        mStatusTextView = (TextView) findViewById(R.id.tv_account_status);
        mPhoneNumberTextView = (TextView) findViewById(R.id.tv_account_phone_number);
        mEmailTextView = (TextView) findViewById(R.id.tv_account_email);
        mGenderTextView = (TextView) findViewById(R.id.tv_account_gender);
        mBirthTextView = (TextView) findViewById(R.id.tv_account_birth);
        mBalanceTextView = (TextView) findViewById(R.id.tv_account_balance);

        mAccountManager = M800SDK.getInstance().getAccountManager();
        mCreditManager = M800SDK.getInstance().getCreditManager();
        mGenderAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, GENDERS);
        mMediaEditingAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                new String[]{"View", "Edit",
                        "DELETE", getString(android.R.string.cancel)});
        mLoadProfileTask.execute();

        mVideoProcessExecutor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mVideoProcessExecutor.shutdownNow();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_cover:
                if (TextUtils.isEmpty(mUserProfile.coverImageURL)) {
                    launchSelectImageActivity("Select Cover Image", REQ_SELECT_COVER);
                } else {
                    showEditCoverDialog();
                }
                break;
            case R.id.iv_profile:
                if (TextUtils.isEmpty(mUserProfile.profileImageURL)) {
                    launchSelectImageActivity("Select Profile Image", REQ_SELECT_PROFILE);
                } else {
                    showEditProfileDialog();
                }
                break;
            case R.id.iv_video_caller_id:
                if (TextUtils.isEmpty(mUserProfile.videoCallerIDURL)) {
                    Toast.makeText(this, "TODO", Toast.LENGTH_SHORT).show();
                } else {
                    showEditVideoDialog();
                }
                break;
            case R.id.tv_account_name:
                launchChangeNameDialog();
                break;
            case R.id.tv_account_status:
                launchChangeStatusDialog();
                break;
            case R.id.tv_account_email:
                launchChangeEmailDialog();
                break;
            case R.id.tv_account_gender:
                launchGenderSelectDialog();
                break;
            case R.id.tv_account_birth:
                launchDatePickerDialog();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            Log.d(DEBUG_TAG, "<onActivityResult> requestCode = " + requestCode + " result != RESULT_OK");
            return;
        }
        switch (requestCode) {
            case REQ_SELECT_COVER:
                mCoverImageView.setOnClickListener(null);
//                SelectFileHelper.handleSelectImageFile(this, data, 256, new ProcessCoverImageCallback());
                break;
            case REQ_SELECT_PROFILE:
                mProfileImageView.setOnClickListener(null);
//                SelectFileHelper.handleSelectImageFile(this, data, 128, new ProcessProfileImageCallback());
                break;
            case REQ_SELECT_VIDEO:
                mVideoCallerThumbImageView.setOnClickListener(null);
                mVideoThumbProgressBar.setVisibility(View.VISIBLE);
                mVideoThumbProgressBar.setIndeterminate(true);
//                mVideoProcessExecutor.submit(new ProcessVideoCallerIDTask(this,
//                        data.getData(), mFFmpeg, new ProcessVideoTaskCallback()));
        }
    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_FILE_UPLOAD_STARTED:
                    if (msg.arg1 == 0) {
                        // Cover image
                        mCoverImageProgressBar.setVisibility(View.VISIBLE);
                        mCoverImageProgressBar.setProgress(0);
                    } else if (msg.arg1 == 1) {
                        // Profile image
                        mProfileImageProgressBar.setVisibility(View.VISIBLE);
                        mProfileImageProgressBar.setProgress(0);
                    } else if (msg.arg1 == 2) {
                        // Video caller ID
                        mVideoThumbProgressBar.setIndeterminate(false);
                    }
                    break;
                case MSG_FILE_UPLOAD_PROGRESS:
                    if (msg.arg1 == 0) {
                        // Cover image
                        mCoverImageProgressBar.setProgress(msg.arg2);
                    } else if (msg.arg1 == 1) {
                        // Profile image
                        mProfileImageProgressBar.setProgress(msg.arg2);
                    } else if (msg.arg1 == 2) {
                        // Video caller ID
                        mVideoThumbProgressBar.setProgress(msg.arg2);
                    }
                    break;
                case MSG_FILE_UPLOAD_ENDED:
                    if (msg.arg1 == 0) {
                        // Cover image
                        mCoverImageProgressBar.setVisibility(View.GONE);
                    } else if (msg.arg1 == 1) {
                        // Profile image
                        mProfileImageProgressBar.setVisibility(View.GONE);
                    } else if (msg.arg1 == 2) {
                        // Video caller ID
                        mVideoThumbProgressBar.setVisibility(View.GONE);
                    }
                    break;
            }
            return true;
        }
    });

    private class MyUserProfile {
        private String coverImageURL;
        private String profileImageURL;
        private String videoCallerIDURL;
        private String videoCallerIDThumbURL;

        private String name;
        private String status;
        private String phoneNumber;
        private String email;
        private IM800UserProfile.Gender gender;
        private String birthDay;
        private String credit;
    }

    private AsyncTask<Void, Void, MyUserProfile> mLoadProfileTask = new AsyncTask<Void, Void, MyUserProfile>() {

        @Override
        protected MyUserProfile doInBackground(Void... params) {
            MyUserProfile myUserProfile = new MyUserProfile();
            myUserProfile.coverImageURL = mAccountManager.getCoverImageUrl();
            myUserProfile.profileImageURL = mAccountManager.getProfileImageUrl();
            myUserProfile.videoCallerIDURL = mAccountManager.getCallerVideoUrl();
            myUserProfile.videoCallerIDThumbURL = mAccountManager.getCallerVideoThumbnailUrl();

            myUserProfile.name = mAccountManager.getName();
            myUserProfile.status = mAccountManager.getStatus();
            myUserProfile.phoneNumber = M800SDK.getInstance().getUserPhoneNumber();
            myUserProfile.email = mAccountManager.getEmailAddress();
            myUserProfile.gender = mAccountManager.getGender();
            myUserProfile.birthDay = mAccountManager.getBirthday();

            myUserProfile.credit = String.valueOf(mCreditManager.getBalance()) + " "
                    + Currency.getInstance(getCurrencyCode(mCreditManager.getCurrencyCode())).getSymbol();
            return myUserProfile;
        }

        @Override
        protected void onPostExecute(MyUserProfile profile) {
            mUserProfile = profile;

            // Load cover image
            if (!TextUtils.isEmpty(profile.coverImageURL)) {
                loadImage(profile.coverImageURL, mCoverImageView);
            }
            // Load profile image
            if (!TextUtils.isEmpty(profile.profileImageURL)) {
                loadImage(profile.profileImageURL, mProfileImageView);
            }
            // Load video caller id thumbnail
            if (!TextUtils.isEmpty(profile.videoCallerIDThumbURL)) {
                loadImage(profile.videoCallerIDThumbURL, mVideoCallerThumbImageView);
            }
            mHeaderNameTextView.setText(profile.name);
            mHeaderStatusTextView.setText(profile.status);
            mNameTextView.setText(profile.name);
            mStatusTextView.setText(profile.status);
            mPhoneNumberTextView.setText(profile.phoneNumber);
            mEmailTextView.setText(profile.email);
            mGenderTextView.setText(profile.gender == null ? "" : profile.gender.name());
            mBirthTextView.setText(profile.birthDay);
            mBalanceTextView.setText(profile.credit);

            // Enable click events
            mCoverImageView.setOnClickListener(AccountActivity.this);
            mProfileImageView.setOnClickListener(AccountActivity.this);
            mVideoCallerThumbImageView.setOnClickListener(AccountActivity.this);

            mNameTextView.setOnClickListener(AccountActivity.this);
            mStatusTextView.setOnClickListener(AccountActivity.this);
            mEmailTextView.setOnClickListener(AccountActivity.this);
            mGenderTextView.setOnClickListener(AccountActivity.this);
            mBirthTextView.setOnClickListener(AccountActivity.this);
        }
    };

    private String getCurrencyCode(int currencyNum) {
        if (currencyNum == 156) {
            return "CNY";
        } else {
            // Do your mapping here
            return "USD";
        }
    }

    private void loadImage(@NonNull String url, @NonNull ImageView imageView) {
        Glide.with(this)
                .load(url)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageView);
    }

    private void showEditCoverDialog() {
        new AlertDialog.Builder(this)
                .setTitle("My Cover Image")
                .setAdapter(mMediaEditingAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) { // View
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setDataAndType(Uri.parse(mUserProfile.coverImageURL), "image/*");
                            startActivity(intent);
                        } else if (which == 1) { // Edit
                            launchSelectImageActivity("Select Cover Image", REQ_SELECT_COVER);
                        } else if (which == 2) { // Delete
                            mAccountManager.deleteCoverImage(new IM800AccountManager
                                    .DeleteUserProfileMediaSourceCallback() {
                                @Override
                                public void complete() {
                                    mUserProfile.coverImageURL = null;
                                    mCoverImageView.setImageDrawable(null);
                                }

                                @Override
                                public void error(M800PacketError error, String message) {
                                    Log.e(DEBUG_TAG, "Failed to delete cover image, message = " + message);
                                }
                            });
                        }
                    }
                })
                .setCancelable(true)
                .show();
    }

    private void showEditProfileDialog() {
        new AlertDialog.Builder(this)
                .setTitle("My Profile Image")
                .setAdapter(mMediaEditingAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setDataAndType(Uri.parse(mUserProfile.profileImageURL), "image/*");
                            startActivity(intent);
                        } else if (which == 1) { // Edit
                            launchSelectImageActivity("Select Profile Image", REQ_SELECT_PROFILE);
                        } else if (which == 2) { // Delete
                            mAccountManager.deleteProfileImage(new IM800AccountManager
                                    .DeleteUserProfileMediaSourceCallback() {
                                @Override
                                public void complete() {
                                    mUserProfile.profileImageURL = null;
                                    mProfileImageView.setImageDrawable(null);
                                }

                                @Override
                                public void error(M800PacketError error, String message) {
                                    Log.e(DEBUG_TAG, "Failed to delete profile image, message = " + message);
                                }
                            });
                        }
                    }
                })
                .setCancelable(true)
                .show();
    }

    private void showEditVideoDialog() {
        new AlertDialog.Builder(this)
                .setTitle("My Video Caller ID")
                .setAdapter(mMediaEditingAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) { // View
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(mUserProfile.videoCallerIDURL));
                            startActivity(intent);
                        } else if (which == 1) { // Edit
//                            launchSelectVideoActivity();
                        } else if (which == 2) { // Delete
                            mAccountManager.deleteCallerVideo(new IM800AccountManager
                                    .DeleteUserProfileMediaSourceCallback() {
                                @Override
                                public void complete() {
                                    mUserProfile.videoCallerIDURL = null;
                                    mUserProfile.videoCallerIDThumbURL = null;
                                    mVideoCallerThumbImageView.setImageDrawable(null);
                                }

                                @Override
                                public void error(M800PacketError error, String message) {
                                    Log.e(DEBUG_TAG, "Failed to delete video, message = " + message);
                                }
                            });
                        }
                    }
                })
                .setCancelable(true)
                .show();
    }

    private void launchSelectImageActivity(String title, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, title), requestCode);
    }

    private void launchChangeNameDialog() {
        mDialogUtils.showDialogWithInputText(new DialogUtils.DialogInfo(R.layout.dialog_with_input_text,
                "Change Name", null, null, mUserProfile.name,
                "OK", new DialogUtils.OnPositiveDialogButtonClickListener() {
            @Override
            public void onClick(EditText editText) {
                final String text = editText.getText().toString();
                mAccountManager.setName(text, new IM800AccountManager.UpdateUserProfileCallback() {
                    @Override
                    public void complete() {
                        mUserProfile.name = text;
                        mHeaderNameTextView.setText(text);
                        mNameTextView.setText(text);
                    }

                    @Override
                    public void error(M800PacketError error, String message) {
                        Log.e(DEBUG_TAG, "<launchChangeNameDialog> failed to set name, message = " + message);
                    }
                });
            }
        }, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }), new DialogUtils.CharacterCountInfo(30, 1, R.string.error_empty_response));
    }

    private void launchChangeStatusDialog() {
        mDialogUtils.showDialogWithInputText(new DialogUtils.DialogInfo(R.layout.dialog_with_input_text,
                "Change Status", null, null, mUserProfile.status,
                "OK", new DialogUtils.OnPositiveDialogButtonClickListener() {
            @Override
            public void onClick(EditText editText) {
                final String text = editText.getText().toString();
                mAccountManager.setStatus(text, new IM800AccountManager.UpdateUserProfileCallback() {
                    @Override
                    public void complete() {
                        mUserProfile.status = text;
                        mHeaderStatusTextView.setText(text);
                        mStatusTextView.setText(text);
                    }

                    @Override
                    public void error(M800PacketError error, String message) {
                        Log.e(DEBUG_TAG, "<launchChangeStatusDialog> failed to set status, message = " + message);
                    }
                });
            }
        }, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }), new DialogUtils.CharacterCountInfo(30, 1, R.string.error_empty_response));
    }

    private void launchChangeEmailDialog() {
        mDialogUtils.showDialogWithInputText(new DialogUtils.DialogInfo(R.layout.dialog_with_input_text,
                "Change Email Address", null, null, mUserProfile.email,
                "OK", new DialogUtils.OnPositiveDialogButtonClickListener() {
            @Override
            public void onClick(EditText editText) {
                final String text = editText.getText().toString();
                mAccountManager.setEmailAddress(text, new IM800AccountManager.UpdateUserProfileCallback() {
                    @Override
                    public void complete() {
                        mUserProfile.email = text;
                        mEmailTextView.setText(text);
                    }

                    @Override
                    public void error(M800PacketError error, String message) {
                        Log.e(DEBUG_TAG, "<launchChangeEmailDialog> failed to set email, message = " + message);
                    }
                });
            }
        }, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }), new DialogUtils.CharacterCountInfo(30, 1, R.string.error_empty_response));
    }


    private void launchGenderSelectDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Change Gender")
                .setCancelable(true)
                .setAdapter(mGenderAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        final IM800UserProfile.Gender gender = GENDERS[which];
                        if (gender != mUserProfile.gender) {
                            mAccountManager.setGender(gender, new IM800AccountManager.UpdateUserProfileCallback() {
                                @Override
                                public void complete() {
                                    Log.d(DEBUG_TAG, "<setGender> complete");
                                    mUserProfile.gender = gender;
                                    mGenderTextView.setText(gender.name());
                                }

                                @Override
                                public void error(M800PacketError error, String message) {
                                    Log.e(DEBUG_TAG, "<setGender> failed, message = " + message);
                                }
                            });
                        }
                    }
                })
                .setNeutralButton(android.R.string.cancel, null)
                .show();
    }


    private void launchDatePickerDialog() {
        DatePickerFragment datePicker = new DatePickerFragment();
        datePicker.setCallback(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {
                mAccountManager.setBirthday(year, monthOfYear, dayOfMonth,
                        new IM800AccountManager.UpdateUserProfileCallback() {
                            @Override
                            public void complete() {
                                mUserProfile.birthDay = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                mBirthTextView.setText(mUserProfile.birthDay);
                            }

                            @Override
                            public void error(M800PacketError error, String message) {
                                Log.e(DEBUG_TAG, "<launchDatePickerDialog> failed to set birthday, message = " + message);
                            }
                        });
            }
        });

        // Set birth day in date picker
        try {
            if (mUserProfile.birthDay != null) {
                String[] date = mUserProfile.birthDay.split("-");
                int year = Integer.valueOf(date[0]);
                int monthOfYear = Integer.valueOf(date[1]) - 1;
                int dayOfMonth = Integer.valueOf(date[2]);
                datePicker.setDate(year, monthOfYear, dayOfMonth);
            }
        } catch (NumberFormatException e) {
            // Ignore
        }
        datePicker.show(getSupportFragmentManager(), "Change Birthday");
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private DatePickerDialog datePickerDialog;
        private int[] mSelectDate;
        private DatePickerDialog.OnDateSetListener mCallback;

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            int year;
            int month;
            int day;
            if (mSelectDate != null) {
                year = mSelectDate[0];
                month = mSelectDate[1];
                day = mSelectDate[2];
            } else {
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            }

            // Create a new instance of DatePickerDialog and return it
            datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            return datePickerDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (mCallback != null) {
                mCallback.onDateSet(view, year, month, day);
            }
        }

        public void setCallback(DatePickerDialog.OnDateSetListener callback) {
            mCallback = callback;
        }

        public void setDate(int year, int month, int day) {
            mSelectDate = new int[]{year, month, day};
            updateDisplay();
        }

        private void updateDisplay() {
            if (datePickerDialog != null && mSelectDate != null) {
                int year = mSelectDate[0];
                int month = mSelectDate[1];
                int day = mSelectDate[2];
                datePickerDialog.updateDate(year, month, day);
            }
        }

    }
}

