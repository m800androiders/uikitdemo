package com.zeromaro.m800uikitdemo.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.m800.sdk.M800SDK;
import com.m800.uikit.util.SharedPrefsHelper;
import com.zeromaro.m800uikitdemo.R;
import com.zeromaro.m800uikitdemo.main.MainActivity;
import com.zeromaro.m800uikitdemo.signup.SignUpActivity;

public class SplashScreenActivity extends AppCompatActivity {

    private final int mDelay = 2000;

    private Handler mSignUpHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mSignUpHandler = new Handler();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Simple delay to imitate loading
        //If user is already Signed-Up -> Goes to Main Activity
        //If user is not Signed-Up -> Goes to Sign Up Activity
        mSignUpHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (M800SDK.getInstance().hasUserSignedUp()) {
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                } else {
                    startActivity(new Intent(SplashScreenActivity.this, SignUpActivity.class));
                }
                finish();
            }
        }, mDelay);
    }

    @Override
    protected void onPause() {
        mSignUpHandler.removeCallbacksAndMessages(null);
        super.onPause();
    }
}
