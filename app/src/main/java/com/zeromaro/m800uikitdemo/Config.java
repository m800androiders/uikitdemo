package com.zeromaro.m800uikitdemo;

/**
 * Created by Aurimas on 24/12/2017.
 */

public class Config {
    public static final String[] M800DefaultVerificationHosts = new String[]{"https://vs-bj.m800.com", "https://ivs.maaii.com"};
    public static final String[] M800DefaultIMHosts = new String[]{"signup-mds.maaii.com:443", "signup-mds-bk.maaii.com:443"};
    public static final String[] M800HTTPSignupHosts = new String[]{"https://signupv2.maaii.com:443"};
    public static final String jpush_appkey = "34223ccc2cfcc0b4cb97da28";
    public static final String google_geo_api_key = "AIzaSyBJGAfQM59y-iRp4pY35HsdBwtgRnZNJA8";
    public static final String file_provider_authority = "com.zeromaro.m800uikitdemo.fileprovider";
    public static final String M800DefaultPhoneApplicationKey = "mapp282d445b-582a-242e-487c-592b75213271";
    public static final String M800DefaultPhoneApplicationSecret = "54252e49-555e-215a-5d79-63265d576179";
    public static final String M800DefaultTabletApplicationKey = "mapp29775b2c-2865-4251-3d58-39673c6c3553";
    public static final String M800DefaultTabletApplicationSecret = "5f48217d-4f6d-7d30-5863-6c4154745d55";
    public static final String M800DefaultDeveloperKey = "mdev5026597b-4648-5276-656e-26333b41352b";
    public static final String M800DefaultDeveloperSecret = "6d31736d-3a6c-2334-4d24-75645871792f";
    public static final String M800DefaultApplicationIdentifier = "com.maaii.mds";
    public static final String M800DefaultApplicationVersion = "1.1.0";
    public static final String M800DefaultCarrier = "mds.maaii.com";
    public static final String M800DefaultCapabilities = "incoming,outgoing";
    public static final String M800DefaultExperation = "10000";
    public static final String M800DefaultTargetServer = "0";
    public static final String M800IMCert = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlFeWpDQ0E3S2dBd0lCQWdJSkFMQUEyQjBuS2VBM01BMEdDU3FHU0liM0RRRUJDd1VBTUlHZU1Rc3dDUVlEDQpWUVFHRXdKRFRqRVdNQlFHQTFVRUNCTU5TRzl1WnlCTGIyNW5JRk5CVWpFU01CQUdBMVVFQnhNSlNHOXVaeUJMDQpiMjVuTVJVd0V3WURWUVFLRXd4Tk9EQXdJRXhwYldsMFpXUXhGakFVQmdOVkJBc1VEVWxVSUNZZ1UyVmpkWEpwDQpkSGt4R0RBV0JnTlZCQU1URDAwNE1EQWdUR2x0YVhSbFpDQkRRVEVhTUJnR0NTcUdTSWIzRFFFSkFSWUxZMkZBDQpiVGd3TUM1amIyMHdIaGNOTVRReE1USXpNVE16TXpReldoY05NelF4TVRFNE1UTXpNelF6V2pDQm5qRUxNQWtHDQpBMVVFQmhNQ1EwNHhGakFVQmdOVkJBZ1REVWh2Ym1jZ1MyOXVaeUJUUVZJeEVqQVFCZ05WQkFjVENVaHZibWNnDQpTMjl1WnpFVk1CTUdBMVVFQ2hNTVRUZ3dNQ0JNYVcxcGRHVmtNUll3RkFZRFZRUUxGQTFKVkNBbUlGTmxZM1Z5DQphWFI1TVJnd0ZnWURWUVFERXc5Tk9EQXdJRXhwYldsMFpXUWdRMEV4R2pBWUJna3Foa2lHOXcwQkNRRVdDMk5oDQpRRzA0TURBdVkyOXRNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQTBhZU1weDZoDQpkRHJkZ1haRXRFL29DaytlMTlpMmI3bEVBdW5NV2JESlY3ZnQxNUpxV3krTVNJYUlkaEs1Y2p6aDZSQWdGVmZIDQpPYU5zYmJVWTI5SWRrd1VaSkZlV2pKakdtYmlSRmF5VmVwWXdycldrc2VSRnFwQXZGY1JLdTJKU1lPTC9SOGRtDQpxeHFtUms5UXl0aGZXSGx0SU05alhqUHVRcU04TVhPSWpGV0RsSURHNDR4TG5mQlFLcVdJSzRrWUZOMEVMWkRQDQpvbStRdk1rdGRsbGZPdURCYWZRZzlYZFpMN3U3L05GZENlTVFGSmcyemNBeWNnWGpXSk9GTVhHd053aEVUZHF5DQpzSGIveTBGakk2ZWtFcWJzbmZrVjEvRWVlVTBnYk94OTJydTlCMjhtTU5rRUg2UENxSURTWnZLenI1RFRjM250DQpNRmx0eEYyVkRYV2tXd0lEQVFBQm80SUJCekNDQVFNd0hRWURWUjBPQkJZRUZCaTFHR2xyM0ZxbjZaK2NqTmNYDQpTYXVqaUdSSU1JSFRCZ05WSFNNRWdjc3dnY2lBRkJpMUdHbHIzRnFuNlorY2pOY1hTYXVqaUdSSW9ZR2twSUdoDQpNSUdlTVFzd0NRWURWUVFHRXdKRFRqRVdNQlFHQTFVRUNCTU5TRzl1WnlCTGIyNW5JRk5CVWpFU01CQUdBMVVFDQpCeE1KU0c5dVp5QkxiMjVuTVJVd0V3WURWUVFLRXd4Tk9EQXdJRXhwYldsMFpXUXhGakFVQmdOVkJBc1VEVWxVDQpJQ1lnVTJWamRYSnBkSGt4R0RBV0JnTlZCQU1URDAwNE1EQWdUR2x0YVhSbFpDQkRRVEVhTUJnR0NTcUdTSWIzDQpEUUVKQVJZTFkyRkFiVGd3TUM1amIyMkNDUUN3QU5nZEp5bmdOekFNQmdOVkhSTUVCVEFEQVFIL01BMEdDU3FHDQpTSWIzRFFFQkN3VUFBNElCQVFCcHAzbVlXMHF6UnJjY0RvTE12NTRJQnFyM0V4dHVGeXM5Rm9ZV2VWOG92MzYwDQpzdS9HckNqQ2ZxZ010aCtUM3F6TVFvVk9NeGQvN3psUjM2dm9HRk9NdVpvTVM2cVliaFFJcWs1bFNtV3QxN3dVDQp2SDJVUEp1RzEvZEtzQ0FvaHRoUTBXZU5rRmtTYXB2VDFPNDJrcFpObWhuQXpCYUVEczN5cXh1MHMrc2FrSmREDQorQStGcExIN1hwdFBaMGFUM29TWVVPblRHdEFEV3MwU0F6a2dqVHhOa0lPNU5IS0xhZURpZFRyeGE2RS9PMllWDQpMZ3ZOWnFxemJubHZPenJWQkJ2cUo0Y2Z1MFJKZ21YZWVuY3RYckZMYkJGd0VLcGZ2S21OOVhhMDczS1UzbllpDQpmRFRCOWE2bmJUNVRYR0Mrbzg3blRTWjJyWm9JdFpsUjcvVlVXQUo2DQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t";
    public static final String M800MqttAppIdentifier = "com.maaii.listen.point.external.mqtt";
    public static final String M800MqttDevKey = "ROqIjNRuppIyAp9_3vQ=";
    public static final String M800MqttDevSecret = "gc1842hz51o=";
}
