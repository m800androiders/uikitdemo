package com.zeromaro.m800uikitdemo.findcontact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.m800.sdk.M800SDK;
import com.m800.sdk.chat.IM800ChatRoom;
import com.m800.sdk.common.M800PacketError;
import com.m800.sdk.contact.IM800ContactManager;
import com.m800.sdk.contact.IM800FindUserManager;
import com.m800.sdk.contact.IM800UserProfile;
import com.m800.sdk.contact.M800AddContactRequest;
import com.m800.uikit.chatroom.M800ChatRoomActivity;
import com.m800.uikit.profile.suc.M800SucProfileActivity;
import com.m800.uikit.profile.userinfo.M800UserInfoActivity;
import com.zeromaro.m800uikitdemo.R;

import java.util.Date;

/**
 * TAKEN FROM M800 SDK DEMO APP with little modifications
 */
public class FindFriendsActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    private EditText mEtPhoneNo;
    private Button mBtnSearch;

    private IM800FindUserManager mFindUserManager;
    private long mRequestStartTime = -1;

    private ConsoleTextView mConsoleLogTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Find friends from server");
        }

        mEtPhoneNo = (EditText) findViewById(R.id.find_friends_et_phone_no);
        mBtnSearch = (Button) findViewById(R.id.find_friends_btn_search);
        mConsoleLogTextView = (ConsoleTextView) findViewById(R.id.find_friends_tv_console_log);

        mEtPhoneNo.addTextChangedListener(this);
        mBtnSearch.setOnClickListener(this);
        mBtnSearch.setEnabled(false);

        mFindUserManager = M800SDK.getInstance().getFindUserManager();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.find_friends_btn_search:
                onSubmit();
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // Ignore
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // Ignore
    }

    @Override
    public void afterTextChanged(Editable editable) {
        mBtnSearch.setEnabled(mEtPhoneNo.length() > 0);
    }

    private void onSubmit() {
        mEtPhoneNo.setEnabled(false);
        mBtnSearch.setEnabled(false);

        mConsoleLogTextView.clear();

        String phoneNumber = mEtPhoneNo.getText().toString();
        mConsoleLogTextView.println("PhoneNumber:" + phoneNumber);
        mConsoleLogTextView.println("");

        mConsoleLogTextView.println("Sending request...");
        mRequestStartTime = System.currentTimeMillis();
        mFindUserManager.findM800UserByPhoneNumberFromServer(phoneNumber, new IM800FindUserManager.FindM800UserByPhoneNumberCallback() {
            @Override
            public void complete(String phoneNumber, IM800UserProfile userProfile, boolean isContact) {
                mConsoleLogTextView.println("Request success, found:" + (userProfile != null));
                mEtPhoneNo.setEnabled(true);
                mBtnSearch.setEnabled(true);
                if (userProfile != null && !TextUtils.isEmpty(userProfile.getJID())) {
                    Intent intent = new Intent(FindFriendsActivity.this, UserInfoActivity.class);
                    intent.putExtra(UserInfoActivity.EXTRA_USER_PROFILE, userProfile);
                    startActivity(intent);
                }
                mConsoleLogTextView.println("Time spent:" + (System.currentTimeMillis() - mRequestStartTime) + "ms");
            }

            @Override
            public void error(String phoneNumber, M800PacketError error, String message) {
                mConsoleLogTextView.println("request failure, error:" + error.name() + " detail:" + message);
                mConsoleLogTextView.println("Time spent:" + (System.currentTimeMillis() - mRequestStartTime) + "ms");
                mEtPhoneNo.setEnabled(true);
                mBtnSearch.setEnabled(true);
            }
        });
    }

}
