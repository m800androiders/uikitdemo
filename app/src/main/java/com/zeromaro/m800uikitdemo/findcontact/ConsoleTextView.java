package com.zeromaro.m800uikitdemo.findcontact;

import android.content.Context;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.widget.TextView;
/**
 * TAKEN FROM M800 SDK DEMO APP
 */
 class ConsoleTextView extends android.support.v7.widget.AppCompatTextView {

    public ConsoleTextView(Context context) {
        super(context);
        init();
    }

    public ConsoleTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init();
    }

    public ConsoleTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setMovementMethod(new ScrollingMovementMethod());
    }

    public void print(final String text) {
        Context context = getContext();
        if (context != null && text != null) {
            Handler handler = new Handler(context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ConsoleTextView.this.append(text);
                }
            });
        }
    }

    public void println(String text) {
        Context context = getContext();
        if (context != null && text != null) {
            print(text + "\n");
        }
    }

    public void clear() {
        Context context = getContext();
        if (context != null) {
            Handler handler = new Handler(context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ConsoleTextView.this.setText("");
                }
            });
        }
    }

}
