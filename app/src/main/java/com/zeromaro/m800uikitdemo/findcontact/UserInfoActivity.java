package com.zeromaro.m800uikitdemo.findcontact;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.m800.sdk.M800SDK;
import com.m800.sdk.call.IM800CallSession;
import com.m800.sdk.call.M800CallSessionManager;
import com.m800.sdk.common.M800PacketError;
import com.m800.sdk.contact.IM800ContactManager;
import com.m800.sdk.contact.IM800FindUserManager;
import com.m800.sdk.contact.IM800UserProfile;
import com.m800.sdk.contact.M800AddContactRequest;
import com.m800.uikit.util.DialogUtils;
import com.zeromaro.m800uikitdemo.R;

import java.io.Serializable;

/**
 * TAKEN FROM M800 SDK DEMO APP with little modifications
 */
public class UserInfoActivity extends AppCompatActivity implements View.OnClickListener, IM800ContactManager.Listener {
    private static final String DEBUG_TAG = UserInfoActivity.class.getSimpleName();

    public static final String EXTRA_USER_PROFILE = "M800_user_profile";
    public static final String EXTRA_ADD_CONTACT_REQUEST = "M800_add_contact_request";
    public static final String EXTRA_USER_JID = "M800_user_jid";

    private ImageView mCoverImageView;
    private ImageView mProfileImageView;
    private ImageView mVideoImageView;
    private View mBlockedIndicator;

    private TextView mNameTextView;
    private TextView mStatusTextView;

    private TextView mEmailTextView;
    private TextView mGenderTextView;
    private TextView mBirthdayTextView;

    private Button mAddFriendButton;
    private View mAcceptAddFriendButton;
    private View mDeclineAddFriendButton;
    private TextView mAddContactReasonTextView;

    private IM800UserProfile mUserProfile;
    private M800AddContactRequest mAddContactRequest;
    private String mJID;
    private boolean isBlocked;
    private String mChatRoomID;
    private IM800ContactManager mContactManager;
    private IM800FindUserManager mUserManager;
    private MenuItem mBlockMenuItem;
    private MenuItem mReportMenuItem;

    private DialogUtils mDialogUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        mDialogUtils = new DialogUtils(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("User info");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null) {
            Serializable serializable = getIntent().getSerializableExtra(EXTRA_USER_PROFILE);
            if (serializable instanceof IM800UserProfile) {
                mUserProfile = (IM800UserProfile) serializable;
            }
            serializable = getIntent().getSerializableExtra(EXTRA_ADD_CONTACT_REQUEST);
            if (serializable instanceof M800AddContactRequest) {
                mAddContactRequest = (M800AddContactRequest) serializable;
            }
        }

        mContactManager = M800SDK.getInstance().getContactManager();
        mUserManager = M800SDK.getInstance().getFindUserManager();

        if (mUserProfile == null && mAddContactRequest == null) {
            Log.e(DEBUG_TAG, "Failed to get IM800UserProfile & mAddContactRequest from intent!");
            finish();
            return;
        }

        // Check if is self
        String selfJID = M800SDK.getInstance().getUserJID();
        mJID = mUserProfile == null ? mAddContactRequest.getJID() : mUserProfile.getJID();
        if (TextUtils.equals(selfJID, mJID)) {
            // Need to prevent user from entering the user profile page of his own.
            // User shouldn't chat with, call to, block or report himself.
            // You can choose to either finish this page or redirect to account page.
            Toast.makeText(this, "You cannot find yourself!", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Find views
        mCoverImageView = (ImageView) findViewById(R.id.iv_cover);
        mProfileImageView = (ImageView) findViewById(R.id.iv_profile);
        mVideoImageView = (ImageView) findViewById(R.id.iv_video_caller_id);
        mBlockedIndicator = findViewById(R.id.iv_block);

        mNameTextView = (TextView) findViewById(R.id.tv_name);
        mStatusTextView = (TextView) findViewById(R.id.tv_status);

        View IMButton = findViewById(R.id.btn_im);
        View onnectCallButton = findViewById(R.id.btn_onnet_call);

        mEmailTextView = (TextView) findViewById(R.id.tv_profile_email);
        mGenderTextView = (TextView) findViewById(R.id.tv_profile_gender);
        mBirthdayTextView = (TextView) findViewById(R.id.tv_profile_birth);

        mAddFriendButton = (Button) findViewById(R.id.btn_add_friend);
        mAcceptAddFriendButton = findViewById(R.id.btn_accept_add_friend);
        mDeclineAddFriendButton = findViewById(R.id.btn_decline_add_friend);
        mAddContactReasonTextView = (TextView) findViewById(R.id.tv_add_friend_reason);

        // Set on click listeners
        IMButton.setOnClickListener(this);
        onnectCallButton.setOnClickListener(this);

        mAddFriendButton.setOnClickListener(this);
        mAcceptAddFriendButton.setOnClickListener(this);
        mDeclineAddFriendButton.setOnClickListener(this);

        // Init views
        initViews();
        invalidateAddContactRequestUI();

        // Update user profile if needed
//        if (mUserProfile == null) {
//            Subscription subscription = UserProfileCache.getInstance().get(mJID)
//                    .subscribe(new Action1<IM800UserProfile>() {
//                        @Override
//                        public void call(IM800UserProfile userProfile) {
//                            if (userProfile != null) {
//                                mUserProfile = userProfile;
//                                initViews();
//                            }
//                        }
//                    });
//            mSubscription.add(subscription);
//            // Reload profile
//            UserProfileCache.getInstance().invalidate(mJID, true);
//        }

        if (mAddContactRequest == null) {
            // Check if this M800 user is current user's contact
            loadContactInfo();
        } // else must be a stranger
    }

    private void initViews() {
        if (mUserProfile == null) {
            Log.d(DEBUG_TAG, "User profile is null, quit <initViews>");
            return;
        }
        loadImage(mUserProfile.getCoverImageURL(), mCoverImageView);
        loadImage(mUserProfile.getProfileImageURL(), mProfileImageView);
        loadImage(mUserProfile.getVideoThumbnailURL(), mVideoImageView);
        mCoverImageView.setOnClickListener(this);
        mProfileImageView.setOnClickListener(this);
        mVideoImageView.setOnClickListener(this);

        isBlocked = mUserProfile.isBlocked();
        mBlockedIndicator.setVisibility(isBlocked ? View.VISIBLE : View.GONE);

        mNameTextView.setText(mUserProfile.getName());
        mStatusTextView.setText(mUserProfile.getStatus());

        mEmailTextView.setText(mUserProfile.getEmailAddress());
        mGenderTextView.setText(mUserProfile.getGender().name());
        mBirthdayTextView.setText(mUserProfile.getBirthday());
    }

    private void invalidateAddContactRequestUI() {
        if (mAddContactRequest != null) {
            if (mAddContactRequest.getDirection() == M800AddContactRequest.Direction.Incoming) {
                mAddFriendButton.setVisibility(View.GONE);
                mAcceptAddFriendButton.setVisibility(View.VISIBLE);
                mDeclineAddFriendButton.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(mAddContactRequest.getReason())) {
                    mAddContactReasonTextView.setText(mAddContactRequest.getReason());
                    mAddContactReasonTextView.setVisibility(View.VISIBLE);
                } else {
                    mAddContactReasonTextView.setVisibility(View.GONE);
                }
            } else if (mAddContactRequest.getDirection() == M800AddContactRequest.Direction.Outgoing) {
                mAddFriendButton.setVisibility(View.VISIBLE);
                mAddFriendButton.setEnabled(false);
                mAddFriendButton.setText("Friend request sent");

                mAcceptAddFriendButton.setVisibility(View.GONE);
                mDeclineAddFriendButton.setVisibility(View.GONE);
                mAddContactReasonTextView.setVisibility(View.GONE);
            }
        } else {
            mAddFriendButton.setVisibility(View.VISIBLE);
            mAddFriendButton.setEnabled(true);
            mAddFriendButton.setText("Add");

            mAcceptAddFriendButton.setVisibility(View.GONE);
            mDeclineAddFriendButton.setVisibility(View.GONE);
            mAddContactReasonTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contact_menu, menu);
        mReportMenuItem = menu.findItem(R.id.menu_report);
        mBlockMenuItem = menu.findItem(R.id.menu_block);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem deleteItem = menu.findItem(R.id.menu_delete);
        deleteItem.setVisible(false);
        if (isBlocked) {
            mBlockMenuItem.setTitle("Unblock");
        } else {
            mBlockMenuItem.setTitle("Block");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_report:
                showReportConfirmDialog();
                return true;
            case R.id.menu_block:
                showBlockConfirmDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContactManager.removeContactChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_cover:
                launchViewImageActivity(mUserProfile.getCoverImageURL());
                break;
            case R.id.iv_profile:
                launchViewImageActivity(mUserProfile.getProfileImageURL());
                break;
            case R.id.iv_video_caller_id:
                launchViewVideoActivity(mUserProfile.getVideoURL());
                break;
            case R.id.btn_im:
                launchChatRoomActivity();
                break;
            case R.id.btn_onnet_call:
                M800CallSessionManager.getInstance().makeOnnetCall(mJID, IM800CallSession.Media.AUDIO);
                break;
            case R.id.btn_add_friend:
                launchAddFriendDialog();
                break;
            case R.id.btn_accept_add_friend:
                mAcceptAddFriendButton.setEnabled(false);
                mDeclineAddFriendButton.setEnabled(false);
                acceptOrRejectAddFriendRequest(true);
                break;
            case R.id.btn_decline_add_friend:
                mAcceptAddFriendButton.setEnabled(false);
                mDeclineAddFriendButton.setEnabled(false);
                acceptOrRejectAddFriendRequest(false);
                break;
        }
    }

    @Override
    public void onQueryRosterStart() {
        // Ignore
    }

    @Override
    public void onContactSyncCompleted(boolean hasChange) {
        // No need for this in new add friend request flow.
        if (hasChange) {
            // Contact changed because of new friend is added, query contact again
            loadContactInfo();
        }
    }

    @Override
    public void onContactSyncError(IM800ContactManager.Error error) {
        // Ignore
    }

    @Override
    public void onNewAddContactRequest(M800AddContactRequest request) {
        // Ignore
    }

    @Override
    public void onAddContactRequestComplete(String jid, M800AddContactRequest.Direction direction, boolean isAccepted) {
        if (TextUtils.equals(mJID, jid)) {
            mContactManager.removeContactChangeListener(this);
            // The add contact request is completed
            if (isAccepted) {
                loadContactInfo();
            } else {
                mAddContactRequest = null;
                invalidateAddContactRequestUI();
            }
        }
    }

    private void loadContactInfo() {
//        Subscription subscription = Observable.fromCallable(
//                new Callable<IM800Contact>() {
//                    @Override
//                    public IM800Contact call() throws Exception {
//                        return mContactManager.findM800ContactByJID(mJID);
//                    }
//                })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<IM800Contact>() {
//                    @Override
//                    public void call(IM800Contact contact) {
//                        if (contact != null) {
//                            Log.d(DEBUG_TAG, "Found IM800Contact with JID " + mJID);
//                            Intent intent = new Intent(M800UserInfoActivity.this, M800ContactInfoActivity.class);
//                            intent.putExtra(M800ContactInfoActivity.EXTRA_M800_CONTACT, contact);
//                            startActivity(intent);
//                            finish();
//                        } else {
//                            Log.i(DEBUG_TAG, "Cannot find IM800Contact with JID " + mJID);
//                            loadAddFriendRequest();
//                        }
//                    }
//                });
//        mSubscription.add(subscription);
    }

    private void acceptOrRejectAddFriendRequest(final boolean accept) {
        M800PacketError error;
        if (accept) {
            error = mContactManager.acceptM800AddContactRequest(mJID);
        } else {
            error = mContactManager.declineM800AddContactRequest(mJID);
        }

        if (error == M800PacketError.NO_ERROR) {
            // Set result ok for AddContactRequestsListActivity
            Intent data = new Intent();
            data.putExtra(EXTRA_USER_JID, mJID);
            setResult(RESULT_OK, data);

            if (accept) {
                Log.d(DEBUG_TAG, "Successfully accepted the request, wait for roster push...");
                mContactManager.addContactChangeListener(UserInfoActivity.this);
            } else {
                Log.d(DEBUG_TAG, "Successfully declined the request");
                mAddContactRequest = null;
                invalidateAddContactRequestUI();
            }
        } else {
            Log.e(DEBUG_TAG, "Failed to accept/decline the request! Error: " + error.name());
            mAcceptAddFriendButton.setEnabled(true);
            mDeclineAddFriendButton.setEnabled(true);
        }


    }

    private void loadAddFriendRequest() {
        M800AddContactRequest request = mContactManager.getAddContactRequest(
                mJID, M800AddContactRequest.Direction.Incoming);
        if (request == null) {
            request = mContactManager.getAddContactRequest(
                    mJID, M800AddContactRequest.Direction.Outgoing);
        }

        if (request != null) {
            mAddContactRequest = request;
            invalidateAddContactRequestUI();
        } else {
            mAddFriendButton.setVisibility(View.VISIBLE);
        }
    }

    private void loadImage(String url, @NonNull ImageView imageView) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Glide.with(this)
                .load(url)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageView);
    }

    private void launchViewImageActivity(String url) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setDataAndType(Uri.parse(url), "image/*");
        startActivity(intent);
    }

    private void launchViewVideoActivity(String url) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    private void launchChatRoomActivity() {
//        if (mChatRoomID != null) {
//            ChatRoomActivity.launch(this, mChatRoomID, IM800ChatRoom.ChatRoomType.SUC);
//            return;
//        }
//        createChatRoom();
    }

    private void createChatRoom() {
//        M800SDK.getInstance().getSingleUserChatRoomManager().createChatRoom(mJID,
//                new IM800SingleUserChatRoomManager.CreateSingleUserChatRoomCallback() {
//                    @Override
//                    public void complete(String roomId, String jid) {
//                        mChatRoomID = roomId;
//                        ChatRoomActivity.launch(M800UserInfoActivity.this,
//                                mChatRoomID, IM800ChatRoom.ChatRoomType.SUC);
//                    }
//
//                    @Override
//                    public void error(String jid, M800ChatRoomError error, String message) {
//                        Log.e(DEBUG_TAG, "Failed to create single chat room, msg = " + message);
//                    }
//                });
    }

    private void launchAddFriendDialog() {
        mDialogUtils.showDialogWithInputText(new DialogUtils.DialogInfo(R.layout.dialog_with_input_text,
                "Add Friend Reason", null, null, null,
                "OK", new DialogUtils.OnPositiveDialogButtonClickListener() {
            @Override
            public void onClick(EditText editText) {
                final String text = editText.getText().toString();
                Log.d(DEBUG_TAG, "<launchAddFriendDialog> note = " + text);
                mAddFriendButton.setEnabled(false);
                addFriend(text);
            }
        }, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }), new DialogUtils.CharacterCountInfo(30, 1, R.string.error_empty_response));
    }

    private void addFriend(final String reason) {
        M800AddContactRequest request = new M800AddContactRequest(mJID, reason,
                M800AddContactRequest.Direction.Outgoing);
        M800PacketError error = mContactManager.requestAddM800Contact(request);
        if (error == M800PacketError.NO_ERROR) {
            mAddContactRequest = request;
            invalidateAddContactRequestUI();
            mContactManager.addContactChangeListener(this);
        } else {
            mAddFriendButton.setEnabled(true);
        }
    }

    private void showBlockConfirmDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Block")
                .setMessage("Block message")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        blockOrUnblockUser();
                    }
                }).setNegativeButton(android.R.string.no, null).setCancelable(true).show();
    }

    private void blockOrUnblockUser() {
        mBlockMenuItem.setEnabled(false);
        if (isBlocked) {
            mUserManager.unblockM800User(mJID, new BlockUserCallback());
        } else {
            mUserManager.blockM800User(mJID, new BlockUserCallback());
        }
    }

    private class BlockUserCallback implements IM800FindUserManager.BlockM800UserCallback {

        @Override
        public void success(String JID, boolean isBlocked) {
            Log.d(DEBUG_TAG, "Block/Unblocked success! isBlocked = " + isBlocked);
            mBlockMenuItem.setEnabled(true);
            UserInfoActivity.this.isBlocked = isBlocked;
            invalidateOptionsMenu();
            mBlockedIndicator.setVisibility(isBlocked ? View.VISIBLE : View.GONE);
        }

        @Override
        public void error(String JID, M800PacketError error, String message) {
            Log.d(DEBUG_TAG, "Failed to block/unblock! Error msg: " + message);
            Toast.makeText(UserInfoActivity.this, "Failed to block/unblock! Error msg: " + message,
                    Toast.LENGTH_SHORT).show();
            mBlockMenuItem.setEnabled(true);
        }
    }

    private void showReportConfirmDialog() {
        mDialogUtils.showDialogWithInputText(new DialogUtils.DialogInfo(R.layout.dialog_with_input_text,
                "Report contact", null, null, null,
                "OK", new DialogUtils.OnPositiveDialogButtonClickListener() {
            @Override
            public void onClick(EditText editText) {
                final String text = editText.getText().toString();
                Log.d(DEBUG_TAG, "<showReportConfirmDialog> reason = " + text);
                reportUser(text);
            }
        }, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }), new DialogUtils.CharacterCountInfo(30, 0, R.string.error_empty_response));
    }

    private void reportUser(String reason) {
        mReportMenuItem.setEnabled(false);
        mUserManager.reportM800User(mJID, reason, new ReportUserCallback());
    }

    private class ReportUserCallback implements IM800FindUserManager.ReportM800UserCallback {

        @Override
        public void success(String JID) {
            Log.d(DEBUG_TAG, "Report success!");
            Toast.makeText(UserInfoActivity.this, "Report success!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void error(String JID, M800PacketError error, String message) {
            Log.e(DEBUG_TAG, "Failed to report user! Error msg: " + message);
            Toast.makeText(UserInfoActivity.this, "Failed to report user! Error msg: " + message,
                    Toast.LENGTH_SHORT).show();
            mReportMenuItem.setEnabled(true);
        }
    }

}
