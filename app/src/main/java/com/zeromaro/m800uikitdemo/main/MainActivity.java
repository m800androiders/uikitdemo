package com.zeromaro.m800uikitdemo.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.m800.uikit.call.M800CallActivity;
import com.m800.uikit.recent.M800RecentActivity;
import com.zeromaro.m800uikitdemo.R;
import com.zeromaro.m800uikitdemo.recent.UIKitDemoRecent;
import com.zeromaro.m800uikitdemo.account.AccountActivity;
import com.zeromaro.m800uikitdemo.findcontact.FindFriendsActivity;
import com.zeromaro.m800uikitdemo.signup.SignUpActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mBtnDemoChats, mBtnOriginalChats, mBtnCall, mBtnAccount, mBtnFindFriends, mBtnLogout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtnDemoChats = (Button) findViewById(R.id.activity_main_btn_overriden_chats);
        mBtnOriginalChats = (Button) findViewById(R.id.activity_main_btn_chats);
        mBtnCall = (Button) findViewById(R.id.activity_main_btn_call);
        mBtnAccount = (Button) findViewById(R.id.activity_main_btn_account);
        mBtnFindFriends = (Button) findViewById(R.id.activity_main_btn_find_friends);
        mBtnLogout = (Button) findViewById(R.id.activity_main_btn_logout);

        mBtnDemoChats.setOnClickListener(this);
        mBtnOriginalChats.setOnClickListener(this);
        mBtnCall.setOnClickListener(this);
        mBtnAccount.setOnClickListener(this);
        mBtnFindFriends.setOnClickListener(this);
        mBtnLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (mBtnDemoChats.getId() == view.getId()) {
            startActivity(new Intent(this, UIKitDemoRecent.class));
        } else if (mBtnOriginalChats.getId() == view.getId()) {
            startActivity(new Intent(this, M800RecentActivity.class));
        } else if (mBtnCall.getId() == view.getId()) {
            startActivity(new Intent(this, M800CallActivity.class));
        } else if (mBtnAccount.getId() == view.getId()) {
            startActivity(new Intent(this, AccountActivity.class));
        } else if (mBtnFindFriends.getId() == view.getId()) {
            startActivity(new Intent(this, FindFriendsActivity.class));
        } else if (mBtnLogout.getId() == view.getId()) {
            /*
            //TODO Signout logic dirty fix, clean DB manually
            //If compiling from source, uncomment. Cleans DB manually on sign out (still need to force quit
            //the app afterwards).
            //If compiling from artifactory, need to clean data of the app manually.

            M800SDK.getInstance().getManagement().signout();
            MaaiiDB.deleteDB(this, new M800DBModuleManager(new DBModule()));
            */

            Intent intent = new Intent(this, SignUpActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}
