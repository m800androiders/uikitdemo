package com.zeromaro.m800uikitdemo;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;

import com.m800.sdk.M800ApplicationPlatform;
import com.m800.sdk.M800SDK;
import com.m800.sdk.M800SDKConfiguration;
import com.m800.sdk.call.CallLogLevels;
import com.m800.sdk.call.M800CallConfiguration;
import com.m800.sdk.call.M800CallSessionManager;
import com.m800.uikit.module.UIKitModuleManager;
import com.m800.verification.LogLevels;
import com.m800.verification.M800VerificationClient;
import com.m800.verification.M800VerificationConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Aurimas on 22/12/2017.
 */

public class Application extends android.app.Application {

    private File mCertFile;
    private File mHoldToneFile;
    private File mRingBackToneFile;

    @Override
    public void onCreate() {
        super.onCreate();

        initM800SDKCore(false);
        initM800SDKCall();
        initM800UIKit();
        initM800Verification();

        registerActivityLifecycleCallbacks(new ActivityLifeCycleCallbacks(getApplicationContext()));
    }

    private void initM800SDKCore(boolean isSecondaryDevice) {
        M800SDKConfiguration configuration = M800SDK.newConfiguration();
        configuration.setApplicationContext(getApplicationContext());

        if (isSecondaryDevice) {
            configuration.setApplicationKey(Config.M800DefaultTabletApplicationKey);
            configuration.setApplicationSecret(Config.M800DefaultTabletApplicationSecret);
            configuration.setApplicationPlatform(M800ApplicationPlatform.AndroidTablet);
        } else {
            configuration.setApplicationKey(Config.M800DefaultPhoneApplicationKey);
            configuration.setApplicationSecret(Config.M800DefaultPhoneApplicationSecret);
            configuration.setApplicationPlatform(M800ApplicationPlatform.AndroidPhone);
        }

        configuration.setApplicationIdentifier(Config.M800DefaultApplicationIdentifier);
        configuration.setApplicationVersion(Config.M800DefaultApplicationVersion);
        configuration.setDeveloperKey(Config.M800DefaultDeveloperKey);
        configuration.setCarrier(Config.M800DefaultCarrier);
        configuration.setCapabilities(Config.M800DefaultCapabilities);
        configuration.setExpiration(Config.M800DefaultExperation);
        configuration.setCertificateForIM(Config.M800IMCert);
        configuration.setDefaultIMHosts(Config.M800DefaultIMHosts);
        configuration.setHTTPSignupHosts(Config.M800HTTPSignupHosts);
        configuration.setSavingCallLogEnabled(true);
        configuration.setSavingConferenceCallLogEnabled(true);
        configuration.setCountingUnreadControlMessageEnabled(false);

        M800SDK.setConfiguration(configuration);
        M800SDK.initialize();
    }

    private void initM800SDKCall() {
        if (mCertFile == null) {
            mCertFile = copyCertificateFromAsset();
            mHoldToneFile = copyHoldToneFromAsset();
            mRingBackToneFile = copyRingBackToneFromAsset();
        }
        M800CallConfiguration config = new M800CallConfiguration.Builder()
                .certificate(mCertFile)
                .holdTone(mHoldToneFile)
                .ringbackTone(mRingBackToneFile)
                .build();

        M800CallSessionManager.setLogLevel(CallLogLevels.DEBUG);
        M800CallSessionManager.init(getApplicationContext(), config);
    }

    private File copyCertificateFromAsset() {
        String assetFileName = "cacert.crt";
        File certFile = copyFileFromAsset(assetFileName);
        return certFile;
    }

    private File copyHoldToneFromAsset() {
        String assetFileName = "hold_tone.raw";
        File holdToneFile = copyFileFromAsset(assetFileName);
        return holdToneFile;
    }

    private File copyRingBackToneFromAsset() {
        String assetFileName = "bell_ringback.raw";
        File ringBackToneFile = copyFileFromAsset(assetFileName);
        return ringBackToneFile;
    }

    private File copyFileFromAsset(String filename) {
        AssetManager am = getAssets();
        InputStream in;
        OutputStream out;
        try {
            in = am.open(filename);
            String cacheDir = getFilesDir().getAbsolutePath();
            File outFile = new File(cacheDir, filename);
            out = new FileOutputStream(outFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }

            in.close();
            out.flush();
            out.close();

            return outFile;
        } catch (IOException e) {
        }
        return null;
    }

    private void initM800UIKit() {
        UIKitModuleManager.initialize(this);
    }

    private void initM800Verification() {
        M800SDKConfiguration m800Config = M800SDK.getConfiguration();

        // Get dev secret
        String devSecret = Config.M800DefaultDeveloperSecret;
        // Get verification service hosts
        String[] verificationHosts = Config.M800DefaultVerificationHosts;
        // Set configuration
        M800VerificationConfiguration.Builder confBuilder =
                new M800VerificationConfiguration.Builder()
                        .applicationKey(m800Config.getApplicationKey())
                        .appSecret(m800Config.getApplicationSecret())
                        .developerKey(m800Config.getDeveloperKey())
                        .devSecret(devSecret)
                        .hosts(new ArrayList<>(Arrays.asList(verificationHosts)));
        M800VerificationClient.setLogLevel(LogLevels.DEBUG);

        M800VerificationClient.getVerificationManager(getApplicationContext()).setConfiguration(confBuilder.build());
    }

    private static class ActivityLifeCycleCallbacks implements ActivityLifecycleCallbacks {
        private WeakReference<Context> mContextWeakReference;

        public ActivityLifeCycleCallbacks(Context context) {
            mContextWeakReference = new WeakReference<Context>(context);
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
            checkIfAppIsInBackground();
        }

        @Override
        public void onActivityStarted(Activity activity) {
        }

        @Override
        public void onActivityResumed(Activity activity) {
        }

        @Override
        public void onActivityPaused(Activity activity) {
        }

        @Override
        public void onActivityStopped(Activity activity) {
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            checkIfAppIsInBackground();
        }

        private boolean isAppIsInBackground() {
            boolean isInBackground = true;
            if (mContextWeakReference.get() == null) {
                return false;
            } else {
                Context context = mContextWeakReference.get();
                ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                    List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                    for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                            for (String activeProcess : processInfo.pkgList) {
                                if (activeProcess.equals(context.getPackageName())) {
                                    isInBackground = false;
                                }
                            }
                        }
                    }
                } else {
                    List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                    ComponentName componentInfo = taskInfo.get(0).topActivity;
                    if (componentInfo.getPackageName().equals(context.getPackageName())) {
                        isInBackground = false;
                    }
                }
            }
            return isInBackground;
        }

        private void checkIfAppIsInBackground() {
            //TODO Any other way to check if app is in the background or foreground?
            if (isAppIsInBackground()) {
                M800SDK.getInstance().getLifeCycleManager().applicationDidEnterBackground();
            } else {
                M800SDK.getInstance().getLifeCycleManager().applicationWillEnterForeground();
            }
        }
    }
}
